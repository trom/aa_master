{# HTML5 essentials #}
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

{# HTML5 meta #}
<title>{{ title | translate("page-title", "Page title") }}</title>
<meta name="description" content="{{ description | translate('page-description', 'Page description') }}">
<link rel="canonical" href="{{ get_canonical_url(page) }}" />

{# http://ogp.me/ #}
<meta property="og:image" content="{{ (og_image if og_image else meta.og_image) | translate('og-image', 'Open graph image')}}">
<meta property="og:title" content="{{ title | translate("page-title") }}">
<meta property="og:description" content="{{ description | translate("page-description") }}">
<meta property="og:locale" content="{{ locale | to_og_locale }}">
{% for alternate_locale in available_locales %}
  {% if alternate_locale != locale %}
    <meta property="og:locale:alternate" content="{{ alternate_locale | to_og_locale }}">
  {% endif %}
{% endfor %}
<meta property="og:url" content="{{ get_canonical_url(page) }}">

{# https://dev.twitter.com/cards/markup #}
<meta name="twitter:card" content="{{ meta.twitter_card }}">
<meta name="twitter:site" content="{{ meta.twitter_site }}">
<meta name="twitter:creator" content="{{ meta.twitter_creator }}">
<meta name="twitter:image" content="{{ (twitter_image if twitter_image else meta.twitter_image) | translate('twitter-image', 'Twitter image') }}">
<meta name="twitter:image:alt" content="{{ (twitter_image_alt if twitter_image_alt else meta.twitter_image_alt) | translate('twitter-image-alt', 'Twitter image alternate text') }}">
