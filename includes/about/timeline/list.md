2006
:   {{ 2006[event] Adblock Plus is written and released. It becomes the most popular Firefox extension in less than a year. }}

2011
:   {{ 2011[event] eyeo, the company behind Adblock Plus, is founded. The Acceptable Ads initiative is born. }}

2012
:   {{ 2012[event] The original Acceptable Ads criteria is developed by eyeo and the Adblock Plus community. By working together, both parties create a standard for better ads that benefits both users and advertisers. }}

2017
:   {{ 2017[event] The Acceptable Ads Committee is formed. eyeo hands over the Acceptable Ads criteria to the Committee. }}

2018
:   {{ 2018[event] The Acceptable Ads Committee releases Mobile Acceptable Ads criteria based on a study by an independent research company. }}

2019 <span class="and-beyond">& beyond</span>
:   {{ 2019[event] The Acceptable Ads Committee turns its attention to video ads and more. }}
