## {{ with-heading[heading] With <span class="group">Acceptable Ads</span> }} {: #advantages .h1 }

{{ with-1 Acceptable Ads create a sustainable middle ground between user choice and monetization. The Acceptable Ads Standard is defined with users in mind. }}

{{ with-2 This helps provide noninvasive ad formats that they are happy to see, allowing publishers to increase their advertising revenue. Everyone wins. }}
